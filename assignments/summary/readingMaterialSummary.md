# IoT Basics

Internet of Things (IoT) is an ecosystem of connected physical objects that are accessible through the internet. The embedded technology in the objects helps them to interact with internal states or the external environment, which in turn affects the decisions taken. IoT enables devices/objects to observe, identify and understand a situation or the surroundings without being dependent on human help.

## Industrial Revolution

- Industry 1.0 (1784) : Mechanization, Steam Power and Weaving loom.
- Industry 2.0 (1870) : Mass Production, Assembly line and Electrical energy.
- Industry 3.0 (1969) : Automation, Computers and Electronics.
- Industry 4.0 (Today) : Cyber Physical Systems, Internet of things and Network.

## Industry 3.0
***
* **Industry 3.0 use Sensors, PLC's, SCADA and ERP.**
* **Workflow:** _Sensors_ --> _PLC_ --> _SCADA & ERP_
* **Architecture of Industry 3.0:**  
	![Architecture](/extras/extras_architecture3.0.png)

* **Communication Protocols:**  
  * The _Sensors_ installed in the factory send data to PLCs by means of _**Field Bus**_, which are basically certain Protocols such as Modbus, CANopen, EtherCAT, etc.

## Industry 4.0

Industry 3.0 + Internet (IoT) = Industry 4.0

<b>Internet</b> sends you data.

#### Architecture:

![4.0 Architecture](/extras/extras_architecture4.0.png)

#### Communication Protocols:

- MQTT.org
- AMQP
- OPC UA
- CoAP RFC 7252
- Websockets
- HTTP
- Restful API

These protocols are optimized for sending data to cloud for data analysis.

### Problems with Industry 4.0 upgrades
 1. Cost
 2. Downtime
 3. Reliability
### Solution

Get data from Industry 3.0 devices/meters/sensors without changes to the original device.

Send the data to the Cloud using Industry 4.0 devices

![Conversion](/extras/extras_conversion.png)

### Challenges in coversion
 - Expensive hardware
 - Lack of documentation
 - Proprietary PLC protocols

 ### Tools used to analyse data:

- <b>IoT TSDB tools:</b> Store your data in Time series databases (eg. Prometheus, InfluxDB)
- <b>IoT Dashboards:</b> View all your data into beautiful dashboards (eg. Grafana, Thingsboard)
- <b>IoT Platforms:</b> Analyse your data on these platforms (eg. AWS IoT, Google IoT, Azure IoT, Thingsboard)

## Applications of Industry 4.0:

![Application](/extras/unnamed.jpg)
